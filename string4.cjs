//let testObject={"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};

function titleCase(testObject){
    if(testObject=== undefined){
        return '';
    }  
    let arr=[];
    for(let key in testObject){
       arr.push(testObject[key].toLowerCase());
    }
  //  console.log(arr);

    for(let index=0; index<arr.length; index++){
        arr[index]=arr[index].charAt(0).toUpperCase()+arr[index].slice(1);
    }
    return arr.join(' ');  
}
module.exports=titleCase;