function numberFormat(givenNumber){
    
    if(givenNumber.charAt(0)!=='$' && givenNumber.charAt(1)!=='$'){
        return Number(0);
    }   
    //  it will replace all except these things 
  let strNumber=givenNumber.replace(/[^0-9.-]/g, "");

  // handle test case with more than one dot
  let countDot=0;
  for(let index=0;index<strNumber.length;index++){
     if(strNumber.charAt(index)==='.'){
        countDot++;
     }
     if(countDot>1){
        return Number(0);
     }
  }
    
  return Number(strNumber);
}
module.exports=numberFormat;