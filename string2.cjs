function ipAdress(ipString){
    let ipArray=[];
   
    if(ipString=== undefined){
        return ipArray;
    }

    let arr=ipString.split('.');
    // invalid
    if(arr.length !==4){
        return ipArray;
    }
    for(let index=0; index<arr.length; index++){
        // isNaN = not a number
        if(isNaN(arr[index])){
            return [];
        }

        if(arr[index]< 0 || arr[index]>255){
            return [];
        }
        
        ipArray.push(Number(arr[index]));

    }

    return ipArray;

}
module.exports=ipAdress;