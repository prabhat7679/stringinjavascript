function findMonth(stringDate){
    let newDateArr=stringDate.split('/');
      // handle test case 
    if(newDateArr[0].length==0 || newDateArr[0].length >2 || newDateArr[1].length==0 || newDateArr[1].length >2
|| newDateArr[2].length!==4){
        return "Invalid Format";
    }
    // handle invalid date fromat
    if(newDateArr[0]>31 || newDateArr[0]<=0){
        return "Invalid Date";
    }
       // handle invalid month test case
     if(newDateArr[1] >12){
        return "Invalid Month";
     }
     if(Number(isNaN(newDateArr[2])=== true)){
         return '';
     }
    let monthArr=['JANUARY', 'FEBRUARY','MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'];
     return monthArr[newDateArr[1]-1];
}
module.exports=findMonth;  
